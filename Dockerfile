#FROM ubuntu:20.04
FROM docker.io/library/stack:v1

WORKDIR /var/www/html

ENV TZ=America/Lima
ENV DATABASE_PASS=urlgen

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN groupadd -g 1000 nginx
RUN useradd -u 1000 -ms /bin/bash -g nginx nginx

RUN echo "----------"
RUN echo "Dockerfile"
RUN echo "----------"

RUN apt-get update -y
RUN apt-get install -y

RUN apt-get install software-properties-common -y
RUN add-apt-repository -y ppa:ondrej/php
RUN apt-get install -y build-essential locales zip vim unzip git curl libzip-dev zip exif
RUN apt-get install -y php8.2-fpm php8.2-common php8.2-pdo php8.2-mysql php8.2-pdo-mysql php8.2-bz2 php8.2-curl php8.2-mbstring php8.2-intl php8.2-gd php8.2-dom php8.2-xml php8.2-bcmath

RUN apt-get clean && rm -rf /var/lib/apt/lists/*

#cp .env.example .env
#
#RUN sed -i '/^DB_DATABASE=/s/=.*/="urlgen"/' .env
#RUN sed -i '/^DB_USERNAME=/s/=.*/="urlgen"/' .env
#RUN sed -i '/^DB_PASSWORD=/s/=.*/="urlgen"/' .env

COPY ./src /var/www/html

COPY --chown=nginx:nginx ./src /var/www/html
USER nginx
EXPOSE 9000


