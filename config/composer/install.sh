#!/bin/sh

echo "-------------------"
echo "composer_install.sh"
echo "-------------------"

composer install

chmod 777 -R /var/www/html

cp .env.example .env

sed -i '/^DB_HOST=/s/=.*/="127.0.0.1"/' .env
sed -i '/^DB_DATABASE=/s/=.*/="urlgen"/' .env
sed -i '/^DB_USERNAME=/s/=.*/="urlgen"/' .env
sed -i '/^DB_PASSWORD=/s/=.*/="urlgen"/' .env

php artisan key:generate
php artisan storage:link

mysql -u urlgen -p urlgen -e "CREATE DATABASE IF NOT EXISTS urlgen"

php artisan migrate:refresh