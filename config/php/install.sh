#!/bin/sh

echo "--------------"
echo "php_install.sh"
echo "--------------"

ln -snf /usr/share/zoneinfo/America/Lima /etc/localtime && echo America/Lima > /etc/timezone

groupadd -g 1000 nginx
useradd -u 1000 -ms /bin/bash -g nginx nginx

apt-get update -y
apt-get install -y
apt-get install software-properties-common -y
add-apt-repository -y ppa:ondrej/php
apt-get update -y
apt-get install -y build-essential
apt-get install -y locales zip vim unzip git curl libzip-dev zip exif   
apt-get install -y php8.2-fpm php8.2-common php8.2-pdo php8.2-mysql php8.2-pdo-mysql php8.2-bz2 php8.2-curl php8.2-mbstring php8.2-intl php8.2-gd php8.2-dom php8.2-xml php8.2-bcmath
apt-get clean && rm -rf /var/lib/apt/lists/*

#./configure --with-mysqli=mysqlnd --with-pdo-mysql=mysqlnd

