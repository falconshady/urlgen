<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class Installer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:installer {--wizard} {--db=} {--user=} {--pass=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Easy-Install wizard command';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->info("---------------------------");
        $this->info("Welcome to wizard installer");
        $this->info("---------------------------");
        $this->info("");

        $wizard = $this->option('wizard');
        $database = $this->option('db');
        $username = $this->option('user');
        $password = $this->option('pass');
        
        if($wizard){
            $database = $this->ask('set DB_DATABASE');
            $username = $this->ask('set DB_USERNAME');
            $password = $this->ask('set DB_PASSWORD');    
        }else{
            if(is_null($database) || is_null($username) || is_null($password)){
                $this->warn("Invalid options");
                return;
            }
        }

        $path = base_path('.env');
        if (!file_exists($path)) {
            exec('cp .env.example .env');
        }

        file_put_contents($path, str_replace(
            'DB_DATABASE=' . env('DB_DATABASE'),
            'DB_DATABASE=' . $database,
            file_get_contents($path)
        ));

        file_put_contents($path, str_replace(
            'DB_USERNAME=' . env('DB_USERNAME'),
            'DB_USERNAME=' . $username,
            file_get_contents($path)
        ));

        file_put_contents($path, str_replace(
            'DB_PASSWORD=' . env('DB_PASSWORD'),
            'DB_PASSWORD=' . $password,
            file_get_contents($path)
        ));

        exec('mysql -u '.$username.' -p'.$password.' -e "CREATE DATABASE IF NOT EXISTS '.$database.'"; ');
        exec("php artisan key:generate");
        exec("php artisan storage:link");
        exec("php artisan migrate:refresh");
        exec("npm i");
        exec("npm run build");

    }
}
