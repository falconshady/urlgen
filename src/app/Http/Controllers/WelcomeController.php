<?php

namespace App\Http\Controllers;

use App\Models\Form;
use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    public function index(Request $request) {
        $data = $request->all();
        
        $row = [
            'nombre' => $data['nombre'] ?? 'No Especificado',
            'apellidos' => $data['apellidos'] ?? 'No Especificado',
            'telefono' => $data['telefono'] ?? 'No Especificado',
            'correo' => $data['correo'] ?? 'No Especificado',
            'imagen' => $data['imagen'] ?? 'No Especificado',
        ];
        
        Form::firstOrNew($row)->save();
        
        return view('welcome', $row);
    }
}
