<?php

namespace App\Http\Controllers;

use App\Http\Requests\SendEmailRequest;

class SendEmailController extends Controller
{
    public function index(SendEmailRequest $request)
    {
        sleep(5);
        return response()->json([
            'success' => true,
            'message' => "Mensaje enviado al correo: ".$request->get('email'),
        ]);
    }
}
