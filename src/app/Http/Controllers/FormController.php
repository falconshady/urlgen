<?php

namespace App\Http\Controllers;

use App\Http\Requests\FormSaveRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class FormController extends Controller
{
    public function index(Request $request) {
        return view('form');
    }
    
    public function generateUrl(FormSaveRequest $request) {
        $data = $request->all();

        foreach ($data as $key => $value){
            if(is_null($data[$key])){
                $data[$key] = '';
            }
        }

        if($request->file('imagen')){
            $file = $request->file('imagen');
            $data['imagen'] = '/storage/'.$file->storeAs('images', $file->getClientOriginalName(), 'public');    
        }
        
        return response()->json([
            'success' => true,
            'url' => '/?'.http_build_query($data)
        ]);
    }
}
