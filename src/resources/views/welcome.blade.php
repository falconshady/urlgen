<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />

        <!-- Styles -->
        @vite('resources/css/app.css')
    </head>
    <body class="bg-gray-100">
        <section class="py-10 md:py-16">
            <div class="container max-w-screen-xl mx-auto px-4">
                <div class="text-center">
                    <div class="flex justify-center mb-8">
                        <img src="{{ ($imagen!='No Especificado') ? $imagen: 'https://static.vecteezy.com/system/resources/previews/009/734/564/large_2x/default-avatar-profile-icon-of-social-media-user-vector.jpg' }}" width="262" alt="Falconshady">
                    </div>
                    <div class="font-medium text-gray-600 text-lg md:text-4xl uppercase mb-2">
                        {{ $nombre }}
                    </div>
                    <div class="font-medium text-gray-600 text-lg md:text-4xl uppercase mb-2">
                        {{ $apellidos }}
                    </div>
                    <div class="font-medium text-gray-600 text-lg md:text-2xl">
                        {{ $telefono }}
                    </div>
                    <div class="font-medium text-gray-600 text-lg md:text-2xl">
                        {{ $correo }}
                    </div>
                </div>
            </div>
        </section>
    </body>
</html>
