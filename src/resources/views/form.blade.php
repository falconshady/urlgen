<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />

        <!-- Styles -->
        @vite('resources/css/app.css')
    </head>
    <body class="bg-gray-100 max-w-7xl mx-auto p-6 lg:p-8">
    <form id="form" name="form" method="post" enctype="multipart/form-data">
        <div class="space-y-12">
            <div class="border-b border-gray-900/10 pb-12">
                <h2 class="text-base font-semibold leading-7 text-gray-900">Generar URL</h2>
                <p class="mt-1 text-sm leading-6 text-gray-600">Ingrese los datos para generar un perfil.</p>
            </div>

            <div class="border-b border-gray-900/10 pb-12">
                <div class="mt-10 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
                    <div class="sm:col-span-3 form-block">
                        <label for="nombre" class="form-block-label">Nombre</label>
                        <div class="mt-2">
                            <input type="text" name="nombre" id="nombre" maxlength="120" class="form-block-input">
                        </div>
                    </div>

                    <div class="sm:col-span-3 form-block">
                        <label for="apellidos" class="form-block-label">Apellidos</label>
                        <div class="mt-2">
                            <input type="text" name="apellidos" id="apellidos" maxlength="120" class="form-block-input">
                        </div>
                    </div>

                    <div class="sm:col-span-3 form-block">
                        <label for="telefono" class="form-block-label">Teléfono</label>
                        <div class="mt-2">
                            <input id="telefono" name="telefono" type="number" maxlength="9" class="form-block-input">
                        </div>
                    </div>

                    <div class="sm:col-span-3 form-block">
                        <label for="correo" class="form-block-label">Correo</label>
                        <div class="mt-2">
                            <input id="correo" name="correo" type="email" maxlength="255" class="form-block-input">
                        </div>
                    </div>

                    <div class="image-selector-content">
                        <div class="col-span-full">
                            <label for="photo" class="form-block-label">Imagen</label>
                            <div class="mt-2 flex items-center gap-x-3">
                                <img id="image-preview" src="https://static.vecteezy.com/system/resources/previews/009/734/564/large_2x/default-avatar-profile-icon-of-social-media-user-vector.jpg" width="100">
                                <div id="button-change">
                                    Cambiar
                                    <input type="file" id="imagen" name="imagen" accept="image/jpeg,image/png">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="button-content">
            <a href="#" id="urlgenlink" target="_blank" class="hidden">Ver URL generada</a>
            <button id="generate" type="submit">Generar</button>
        </div>
    </form>

    <script type="text/javascript">
        function $el(selector){
            return document.querySelector(selector);
        }
        
        window.onload = function(event) {

            $el('#imagen').onchange = function (event){
                var reader = new FileReader();
                reader.onload = function(){
                    $el('#image-preview').src = reader.result;
                };
                reader.readAsDataURL(event.target.files[0]);
            }
            
            const form = document.querySelector('form');
            form.addEventListener('submit', event => {
                event.preventDefault();
                
                var data = new FormData($el('#form'))

                fetch('/generate-url', {
                    headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                    method: 'POST',
                    body: data,
                })
                .then(res => res.json())
                .then(function (data) {
                    $el('#urlgenlink').href = data.url
                    $el('#urlgenlink').classList.remove('hidden')
                }).catch(function (error) {
                    console.warn(error);
                });
            })
            
        };
    </script>
    
    </body>
</html>
